package hu.upscale.message.store.ignite;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MessageLogTest {

  private static final Path I_NODE_0_MSG_LOG = Paths.get("./iNode0-message-log.log").toAbsolutePath().normalize();
  private static final Path I_NODE_1_MSG_LOG = Paths.get("./iNode1-message-log.log").toAbsolutePath().normalize();
  private static final Path I_NODE_2_MSG_LOG = Paths.get("./iNode2-message-log.log").toAbsolutePath().normalize();

  private static final Path I_NODE_0_MSG_COUNT = Paths.get("./iNode0" + "-message-count.log").toAbsolutePath()
      .normalize();
  private static final Path I_NODE_1_MSG_COUNT = Paths.get("./iNode1" + "-message-count.log").toAbsolutePath()
      .normalize();
  private static final Path I_NODE_2_MSG_COUNT = Paths.get("./iNode2" + "-message-count.log").toAbsolutePath()
      .normalize();

  private static final List<String> I_NODE_0_LINES = new ArrayList<>();
  private static final List<String> I_NODE_1_LINES = new ArrayList<>();
  private static final List<String> I_NODE_2_LINES = new ArrayList<>();

  private static long iNode0MessageCount = 0;
  private static long iNode1MessageCount = 0;
  private static long iNode2MessageCount = 0;

  @BeforeClass
  public static void init() {
    var f00 = readAllLines(I_NODE_0_MSG_LOG).thenAccept(I_NODE_0_LINES::addAll);
    var f01 = readAllLines(I_NODE_1_MSG_LOG).thenAccept(I_NODE_1_LINES::addAll);
    var f02 = readAllLines(I_NODE_2_MSG_LOG).thenAccept(I_NODE_2_LINES::addAll);

    var f10 = readAllText(I_NODE_0_MSG_COUNT).thenAccept(str -> iNode0MessageCount = Long.parseLong(str));
    var f11 = readAllText(I_NODE_1_MSG_COUNT).thenAccept(str -> iNode1MessageCount = Long.parseLong(str));
    var f12 = readAllText(I_NODE_2_MSG_COUNT).thenAccept(str -> iNode2MessageCount = Long.parseLong(str));

    CompletableFuture.allOf(f00, f01, f02, f10, f11, f12).join();
  }

  private static CompletableFuture<List<String>> readAllLines(Path path) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return Files.readAllLines(path);
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    });
  }

  private static CompletableFuture<String> readAllText(Path path) {
    return CompletableFuture.supplyAsync(() -> {
      try {
        return Files.readString(path);
      } catch (IOException e) {
        throw new IllegalStateException(e);
      }
    });
  }

  @Test
  public void repeatedMessagesNode0() {
    Map<String, Integer> consumption = new HashMap<>();
    I_NODE_0_LINES.forEach(uuid -> consumption.compute(uuid, (key, value) -> value == null ? 1 : value + 1));
    Assert.assertEquals(0,
        consumption.entrySet().stream().filter(entry -> entry.getValue() > 1).peek(System.err::println).count());
  }

  @Test
  public void repeatedMessagesNode1() {
    Map<String, Integer> consumption = new HashMap<>();
    I_NODE_1_LINES.forEach(uuid -> consumption.compute(uuid, (key, value) -> value == null ? 1 : value + 1));
    Assert.assertEquals(0,
        consumption.entrySet().stream().filter(entry -> entry.getValue() > 1).peek(System.err::println).count());
  }

  @Test
  public void repeatedMessagesNode2() {
    Map<String, Integer> consumption = new HashMap<>();
    I_NODE_2_LINES.forEach(uuid -> consumption.compute(uuid, (key, value) -> value == null ? 1 : value + 1));
    Assert.assertEquals(0,
        consumption.entrySet().stream().filter(entry -> entry.getValue() > 1).peek(System.err::println).count());
  }

  @Test
  public void repeatedMessagesOverall() {
    Map<String, Integer> consumption = new HashMap<>();
    Stream.of(I_NODE_0_LINES, I_NODE_1_LINES, I_NODE_2_LINES)
        .flatMap(Collection::stream)
        .forEach(uuid -> consumption.compute(uuid, (key, value) -> value == null ? 1 : value + 1));

    Assert.assertEquals(0,
        consumption.entrySet().stream().filter(entry -> entry.getValue() > 1).peek(System.err::println).count());
    ;

  }

  @Test
  public void totalNumberOfMessages() {
    Assert.assertEquals(iNode0MessageCount + iNode1MessageCount + iNode2MessageCount,
        I_NODE_0_LINES.size() + I_NODE_1_LINES.size() + I_NODE_2_LINES.size());
  }

}
