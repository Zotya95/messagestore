package hu.upscale.message.store.ignite;

import hu.upscale.message.store.ignite.controller.MessageStoreController;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/")
public class JerseyConfig extends ResourceConfig {

  public JerseyConfig() {
    super(MessageStoreController.class);
  }
}
