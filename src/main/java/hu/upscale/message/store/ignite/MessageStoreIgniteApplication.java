package hu.upscale.message.store.ignite;

import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import hu.upscale.message.store.ignite.cassandra.CassandraAddressTranslator;
import hu.upscale.message.store.ignite.cassandra.CassandraRetryUntilSuccessPolicy;
import hu.upscale.message.store.ignite.model.Message;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.PriorityQueue;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteSpring;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CachePeekMode;
import org.apache.ignite.cache.eviction.lru.LruEvictionPolicyFactory;
import org.apache.ignite.cache.store.cassandra.CassandraCacheStoreFactory;
import org.apache.ignite.cache.store.cassandra.datasource.DataSource;
import org.apache.ignite.cache.store.cassandra.persistence.KeyValuePersistenceSettings;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.logger.slf4j.Slf4jLogger;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.spring.SpringTransactionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,
    DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableScheduling
public class MessageStoreIgniteApplication implements AsyncConfigurer {

  private static final Logger log = LoggerFactory.getLogger(MessageStoreIgniteApplication.class);
  public static final String MESSAGE_CACHE_NAME = "message-cache";

  @Bean
  @Override
  public ThreadPoolTaskExecutor getAsyncExecutor() {
    ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
    pool.setThreadNamePrefix("message-store-thread");
    pool.setThreadGroupName("message-store-thread-group");
    pool.setCorePoolSize(Runtime.getRuntime().availableProcessors() * 4);
    pool.setMaxPoolSize(Runtime.getRuntime().availableProcessors() * 8);
    pool.setWaitForTasksToCompleteOnShutdown(true);
    return pool;
  }

  @Bean("igniteInstanceName")
  public String igniteInstanceName() {
    return System.getProperty("igniteInstanceName");
  }

  @Bean
  public IgniteConfiguration igniteConfiguration(String igniteInstanceName) {
    // Setting Ignite node.
    return new IgniteConfiguration()
        .setIgniteInstanceName(igniteInstanceName)
        .setGridLogger(new Slf4jLogger());
  }

  @Bean(destroyMethod = "close")
  public Ignite ignite(IgniteConfiguration igniteConfiguration, ApplicationContext applicationContext)
      throws IgniteCheckedException {
    // Ignite instance
    return IgniteSpring.start(igniteConfiguration, applicationContext);
  }

  @Bean
  public IgniteCompute igniteCompute(Ignite ignite) {
    return ignite.compute();
  }

  @Bean
  public DataSource cassandraDataSource() {
    DataSource dataSource = new DataSource();

    dataSource.setUser("cassandra");
    dataSource.setPassword("cassandra");
    dataSource.setContactPoints(System.getProperty("cassandraAddress"));
    dataSource.setReadConsistency("QUORUM");
    dataSource.setWriteConsistency("QUORUM");
    dataSource.setLoadBalancingPolicy(
        DCAwareRoundRobinPolicy.builder()
            .withLocalDc(System.getProperty("localCassandraDataCenter"))
            .withUsedHostsPerRemoteDc(3)
            .build());
    dataSource.setRetryPolicy(new CassandraRetryUntilSuccessPolicy());
    dataSource.setCollectMetrix(true);
    dataSource.setJmxReporting(true);
    dataSource.setAddressTranslator(new CassandraAddressTranslator());

    return dataSource;
  }

  @Bean
  public KeyValuePersistenceSettings cassandraMessageKeyValuePersistenceSettings() {
    return new KeyValuePersistenceSettings(new ClassPathResource("cassandra-persistence-descriptor.xml"));
  }

  @Bean
  public CassandraCacheStoreFactory<Integer, PriorityQueue<Message>> cassandraMessageCacheStoreFactory(
      DataSource cassandraDataSource,
      KeyValuePersistenceSettings cassandraKeyValuePersistenceSettings) {
    return new CassandraCacheStoreFactory<Integer, PriorityQueue<Message>>()
        .setDataSource(cassandraDataSource)
        .setPersistenceSettings(cassandraKeyValuePersistenceSettings);
  }

  @Bean(destroyMethod = "close")
  public IgniteCache<Integer, PriorityQueue<Message>> messageCache(Ignite ignite,
      CassandraCacheStoreFactory<Integer, PriorityQueue<Message>> cassandraMessageCacheStoreFactory) {

    LruEvictionPolicyFactory<Integer, PriorityQueue<Message>> onHeapEvictionPolicyFactory = new LruEvictionPolicyFactory<>();
    onHeapEvictionPolicyFactory.setMaxMemorySize(1L * 1024L * 1024L * 1024L);

    // Defining and creating a new cache to be used by Ignite.
    CacheConfiguration<Integer, PriorityQueue<Message>> cacheConfiguration =
        new CacheConfiguration<Integer, PriorityQueue<Message>>(MESSAGE_CACHE_NAME)
            .setStatisticsEnabled(true)

            .setWriteThrough(true)
            .setReadThrough(false)
            .setCacheStoreFactory(cassandraMessageCacheStoreFactory)

            .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
            .setCacheMode(CacheMode.PARTITIONED)
            .setBackups(2)

            // Enabling on-heap caching for this distributed cache.
            .setOnheapCacheEnabled(true)
            // Set the maximum cache memory size.
            .setEvictionPolicyFactory(onHeapEvictionPolicyFactory);

    IgniteCache<Integer, PriorityQueue<Message>> cache = ignite.getOrCreateCache(cacheConfiguration);
    if (cache.size(CachePeekMode.ALL) == 0) {
      cache.loadCache(null);
    }

    return cache;
  }

  @Bean(name = "igniteTransactionManager")
  @DependsOn("ignite")
  public SpringTransactionManager igniteTransactionManager(ApplicationContext applicationContext,
      String igniteInstanceName) {
    // Setting Spring transactions for ignite
    SpringTransactionManager springTransactionManager = new SpringTransactionManager();
    springTransactionManager.setApplicationContext(applicationContext);
    springTransactionManager.setIgniteInstanceName(igniteInstanceName);
    springTransactionManager.setTransactionConcurrency(TransactionConcurrency.PESSIMISTIC);

    return springTransactionManager;
  }

  @Bean
  public SecureRandom secureRandom() throws NoSuchAlgorithmException {
    return SecureRandom.getInstanceStrong();
  }

  public static void main(String[] args) {
    SpringApplication.run(MessageStoreIgniteApplication.class, args);
  }

  public static class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
      if (log.isErrorEnabled()) {
        log.error(
            String.format("Uncaught async exception from method: [%s]. The method was called with parameters: [%s]",
                method.getName(), Arrays.asList(params)), ex);
      }
    }
  }
}
