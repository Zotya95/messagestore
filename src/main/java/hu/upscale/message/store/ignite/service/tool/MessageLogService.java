package hu.upscale.message.store.ignite.service.tool;

import hu.upscale.message.store.ignite.dto.MessageStoreResults;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessageLogService implements DisposableBean {

  private final String igniteInstanceName;
  private final AtomicInteger numberOfIncomingMessages = new AtomicInteger(0);
  private final List<UUID> consumedMessages = Collections.synchronizedList(new ArrayList<>());

  @Async
  public CompletableFuture<Void> addMessageUUIDToLog(UUID messageUUID) {
    consumedMessages.add(messageUUID);
    return CompletableFuture.completedFuture(null);
  }

  @Async
  public CompletableFuture<MessageStoreResults> getMessageStoreResults() {
    return CompletableFuture.completedFuture(new MessageStoreResults(new ArrayList<>(consumedMessages),
        numberOfIncomingMessages.intValue()));
  }

  @Override
  public void destroy() throws Exception {
    Files.write(
        Paths.get("./" + igniteInstanceName + "-message-log.log").toAbsolutePath().normalize(),
        consumedMessages.stream().map(UUID::toString).collect(Collectors.toSet()),
        StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING
    );

    Files.write(
        Paths.get("./" + igniteInstanceName + "-message-count.log").toAbsolutePath().normalize(),
        numberOfIncomingMessages.toString().getBytes(Charset.forName("UTF-8")),
        StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING
    );
  }

  @Async
  public CompletableFuture<Integer> increaseMessageCounter() {
    return CompletableFuture.completedFuture(numberOfIncomingMessages.incrementAndGet());
  }
}
