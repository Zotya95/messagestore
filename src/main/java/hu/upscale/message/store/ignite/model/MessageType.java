package hu.upscale.message.store.ignite.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class MessageType implements Serializable {

  private static final long serialVersionUID = 0L;

  private final String name;
  private final int maxNumberOfAttempts;
  private final int ttlOfMessageTypeLock;
  private final boolean strictlySequential;

}
