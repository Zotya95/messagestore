package hu.upscale.message.store.ignite.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.WriteType;
import com.datastax.driver.core.exceptions.BusyConnectionException;
import com.datastax.driver.core.exceptions.BusyPoolException;
import com.datastax.driver.core.exceptions.ConnectionException;
import com.datastax.driver.core.exceptions.DriverException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.TraceRetrievalException;
import com.datastax.driver.core.policies.RetryPolicy;

public class CassandraRetryUntilSuccessPolicy implements RetryPolicy {

  @Override
  public RetryDecision onReadTimeout(Statement statement, ConsistencyLevel cl, int requiredResponses,
      int receivedResponses, boolean dataRetrieved, int nbRetry) {
    return RetryDecision.retry(cl);
  }

  @Override
  public RetryDecision onWriteTimeout(Statement statement, ConsistencyLevel cl, WriteType writeType, int requiredAcks,
      int receivedAcks, int nbRetry) {

    return WriteType.COUNTER == writeType ? RetryDecision.rethrow() : RetryDecision.retry(cl);
  }

  @Override
  public RetryDecision onUnavailable(Statement statement, ConsistencyLevel cl, int requiredReplica, int aliveReplica,
      int nbRetry) {
    return RetryDecision.retry(cl);
  }

  @Override
  public RetryDecision onRequestError(Statement statement, ConsistencyLevel cl, DriverException e, int nbRetry) {

    if (matchAny(e.getClass(),
        QueryExecutionException.class,
        TraceRetrievalException.class)) {
      return RetryDecision.retry(cl);
    }

    if (matchAny(e.getClass(),
        BusyConnectionException.class,
        BusyPoolException.class,
        ConnectionException.class,
        NoHostAvailableException.class)) {
      return RetryDecision.tryNextHost(cl);
    }

    return RetryDecision.rethrow();
  }

  @Override
  public void init(Cluster cluster) {
    // Nothing to do
  }

  @Override
  public void close() {
    // Nothing to do
  }

  private boolean matchAny(Object subject, Object... candidates) {
    for (Object candidate : candidates) {
      if (subject.equals(candidate)) {
        return true;
      }
    }
    return false;
  }

}
