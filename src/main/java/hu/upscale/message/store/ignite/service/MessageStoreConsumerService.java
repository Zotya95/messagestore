package hu.upscale.message.store.ignite.service;

import static hu.upscale.message.store.ignite.MessageStoreIgniteApplication.MESSAGE_CACHE_NAME;

import hu.upscale.message.store.ignite.job.ConsumerJob;
import lombok.AllArgsConstructor;
import org.apache.ignite.IgniteCompute;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessageStoreConsumerService {

  private final MessageStoreService messageStoreService;
  private final IgniteCompute igniteCompute;

  @Async
  @Scheduled(initialDelay = 10_000L, fixedRate = 1000L)
  public void consumeMessages() {
    messageStoreService.getMessagePartitionHashes().thenAccept(messagePartitions -> messagePartitions
        .forEach(messagePartition -> igniteCompute
            .affinityRun(MESSAGE_CACHE_NAME, messagePartition, new ConsumerJob(messagePartition))));

  }

}
