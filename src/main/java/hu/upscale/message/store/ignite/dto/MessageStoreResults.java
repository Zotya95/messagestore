package hu.upscale.message.store.ignite.dto;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MessageStoreResults {

  private List<UUID> consumedMessages;
  private int numberOfIncomingMessages;

  public int getNumberOfFailedOrUnknownMessages() {
    return numberOfIncomingMessages - consumedMessages.size();
  }

}
