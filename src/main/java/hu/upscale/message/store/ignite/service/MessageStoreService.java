package hu.upscale.message.store.ignite.service;

import hu.upscale.message.store.ignite.dto.MessageStoreStatistics;
import hu.upscale.message.store.ignite.model.Message;
import hu.upscale.message.store.ignite.model.MessagePartition.MessageStatus;
import hu.upscale.message.store.ignite.service.tool.MessageLogService;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.cache.Cache;
import lombok.AllArgsConstructor;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cache.query.ScanQuery;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional("igniteTransactionManager")
public class MessageStoreService {

  private final MessageLogService messageLogService;
  private final IgniteCache<Integer, PriorityQueue<Message>> messageCache;

  @Async
  public CompletableFuture<Set<Integer>> getMessagePartitionHashes() {

    List<Integer> keys = messageCache.query(new ScanQuery<Integer, PriorityQueue<Message>>(
            // Remote filter.
            (k, p) -> p != null && p.stream().anyMatch(message -> MessageStatus.PENDING == message.getMessageStatus())),
        // Transformer.
        Cache.Entry::getKey).getAll();
    return CompletableFuture.completedFuture(new HashSet<>(keys));
  }

  @Async
  public CompletableFuture<Void> enqueueMessage(Message message) {
    int messagePartitionHash = message.getMessagePartitionHash();
    PriorityQueue<Message> messages = Optional.ofNullable(messageCache.get(messagePartitionHash))
        .orElseGet(PriorityQueue::new);
    messages.add(message);
    messageCache.put(messagePartitionHash, messages);

    return CompletableFuture.completedFuture(null);
  }

  @Async
  public CompletableFuture<Map<Integer, PriorityQueue<Message>>> getMessageQueues() {
    Map<Integer, PriorityQueue<Message>> messageQueues = messageCache
        .query(new ScanQuery<Integer, PriorityQueue<Message>>(null)).getAll().stream()
        .collect(Collectors.toMap(Cache.Entry::getKey, Cache.Entry::getValue));
    return CompletableFuture.completedFuture(messageQueues);
  }

  @Async
  public CompletableFuture<Void> updateMessageQueue(final int messagePartitionHash,
      Consumer<PriorityQueue<Message>> messageQueueConsumer) {
    PriorityQueue<Message> messageQueue = messageCache.get(messagePartitionHash);
    if (messageQueue != null) {
      messageQueueConsumer.accept(messageQueue);
      messageCache.put(messagePartitionHash, messageQueue);
      messageQueue.stream().map(message -> messageLogService.addMessageUUIDToLog(message.getUuid()))
          .forEach(CompletableFuture::join);
    }
    return CompletableFuture.completedFuture(null);
  }

  @Async
  public CompletableFuture<Void> clear() {
    messageCache.removeAll();
    return CompletableFuture.completedFuture(null);
  }

  @Async
  public CompletableFuture<MessageStoreStatistics> getMessageStoreStatistics() {
    Map<MessageStatus, Long> numberOfMessagesByStatus = messageCache
        .query(new ScanQuery<Integer, PriorityQueue<Message>>(null),
            entry -> entry.getValue().stream()
                .collect(Collectors.groupingBy(Message::getMessageStatus, Collectors.counting())))
        .getAll()
        .stream()
        .flatMap(map -> map.entrySet().stream())
        .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.summingLong(Map.Entry::getValue)));

    return CompletableFuture.completedFuture(new MessageStoreStatistics(numberOfMessagesByStatus));
  }

  @Async
  public CompletableFuture<CacheMetrics> getMessageStoreMetrics() {
    return CompletableFuture.completedFuture(messageCache.metrics());
  }

}
