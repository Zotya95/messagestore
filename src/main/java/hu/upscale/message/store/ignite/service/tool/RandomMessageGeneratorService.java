package hu.upscale.message.store.ignite.service.tool;

import hu.upscale.message.store.ignite.model.Message;
import hu.upscale.message.store.ignite.model.MessagePartition.MessageStatus;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RandomMessageGeneratorService {

  private static final List<String> messageTypes = Arrays.asList("HUF_TRANSFER", "CARD_CONTROL_SMS", "BALANCE_SMS");

  private final SecureRandom random;

  private String generateRandomMessageType() {
    return messageTypes.get(random.nextInt(messageTypes.size()));
  }

  private int generateRandomProcessingPartition() {
    return random.nextInt(64) + 1;
  }

  private String generateRandomTransactionId(String messageTypeName) {
    if (messageTypeName.contains("TRANSFER")) {
      return IntStream.concat(IntStream.of(random.nextInt(9) + 1),
          random.ints(9, 0, 10))
          .mapToObj(Integer::toString)
          .collect(Collectors.joining());

    }
    return null;
  }

  private String generateRandomMessage() {
    byte[] messageArray = new byte[random.nextInt(800) + 200];
    random.nextBytes(messageArray);
    return Base64.getEncoder().encodeToString(messageArray);
  }

  public Message generateRandom() {
    String messageType = generateRandomMessageType();
    return Message.builder()
        .messageStatus(MessageStatus.PENDING)
        .messageType(messageType)
        .processingPartition(generateRandomProcessingPartition())
        .priority((byte) random.nextInt(8))
        .transactionId(generateRandomTransactionId(messageType))
        .messageBody(generateRandomMessage())
        .build();
  }

}
