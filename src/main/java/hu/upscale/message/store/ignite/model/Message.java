package hu.upscale.message.store.ignite.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Message extends MessagePartition implements Comparable<Message> {

  private static final long serialVersionUID = 0L;

  private UUID uuid = UUID.randomUUID();
  private MessageStatus messageStatus;
  private byte priority;
  private LocalDateTime executionTimestamp;
  private long delay;
  private String transactionId;
  private int numberOfAttempts;
  private LocalDateTime lastUpdateTimestamp;
  private String messageBody;

  @Builder
  public Message(String messageType, int processingPartition,
      MessageStatus messageStatus, byte priority, LocalDateTime executionTimestamp, long delay,
      String transactionId, String messageBody) {
    super(messageType, processingPartition);
    LocalDateTime now = LocalDateTime.now();
    this.messageStatus = messageStatus == null ? MessageStatus.PENDING : messageStatus;
    this.priority = priority;
    this.executionTimestamp = executionTimestamp == null ? now : executionTimestamp;
    this.delay = delay;
    this.transactionId = transactionId;
    lastUpdateTimestamp = now;
    this.messageBody = messageBody;
  }

  @Override
  public int compareTo(@NotNull Message o) {
    return executionTimestamp.compareTo(o.executionTimestamp);
  }
}
