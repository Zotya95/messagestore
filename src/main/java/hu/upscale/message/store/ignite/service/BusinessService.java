package hu.upscale.message.store.ignite.service;

import hu.upscale.message.store.ignite.model.Message;
import hu.upscale.message.store.ignite.model.MessagePartition.MessageStatus;
import java.time.LocalDateTime;
import java.util.PriorityQueue;
import java.util.concurrent.CompletableFuture;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service("businessService")
@AllArgsConstructor
public class BusinessService {

  private static final Logger log = LoggerFactory.getLogger(BusinessService.class);

  private final MessageStoreService messageStoreService;

  private void consumeMessage(Message message) {
    try {
      //Thread.sleep(random.nextInt(1751) + 250L);
      //message.setMessageStatus(random.nextInt(100) + 1 > 80 ? MessageStatus.FAILED : MessageStatus.PROCESSED);
      message.setMessageStatus(MessageStatus.PROCESSED);
      message.setNumberOfAttempts(message.getNumberOfAttempts() + 1);
      message.setLastUpdateTimestamp(LocalDateTime.now());
    } catch (Exception e) {
      log.error("Message consumption interrupted", e);
      //Thread.currentThread().interrupt();
    }
  }

  private void consumeMessageQueue(PriorityQueue<Message> messageQueue) {
    messageQueue.stream()
        .filter(message -> MessageStatus.PENDING == message.getMessageStatus())
        .forEach(this::consumeMessage);
  }

  @Async
  public CompletableFuture<Void> consumeMessageQueue(int messagePartitionHash) {
    return messageStoreService.updateMessageQueue(messagePartitionHash, this::consumeMessageQueue);
  }

}
