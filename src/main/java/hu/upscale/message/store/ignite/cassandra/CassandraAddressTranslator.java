package hu.upscale.message.store.ignite.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.policies.AddressTranslator;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CassandraAddressTranslator implements AddressTranslator {

  private static final Logger log = LoggerFactory.getLogger(CassandraAddressTranslator.class);

  private final Properties addressTranslations = new Properties();

  public CassandraAddressTranslator() {
    String propertiesFilePath = System.getProperty("cassandraAddressTranslationsProps");
    if (propertiesFilePath != null && !propertiesFilePath.isEmpty()) {
      try (InputStream is = new FileInputStream(propertiesFilePath)) {
        addressTranslations.load(is);
      } catch (IOException e) {
        log.error("Failed to load Cassandra address translations from properties file: [{}]", propertiesFilePath);
      }
    }
  }

  @Override
  public void init(Cluster cluster) {
    // Nothing to do
  }

  @Override
  public InetSocketAddress translate(InetSocketAddress address) {
    String translation = addressTranslations.getProperty(address.getAddress().getHostAddress() + ':' + address.getPort());
    if (translation != null) {
      translation = translation.trim();
      if (translation.matches("[^:]+:\\d+")) {
        String[] inetSocketAddressParts = translation.split(":");
        return InetSocketAddress.createUnresolved(inetSocketAddressParts[0], Integer.parseInt(inetSocketAddressParts[1]));
      }
    }
    return null;
  }

  @Override
  public void close() {
    // Nothing to do
  }

}
