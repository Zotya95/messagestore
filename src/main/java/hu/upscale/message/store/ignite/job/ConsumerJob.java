package hu.upscale.message.store.ignite.job;

import hu.upscale.message.store.ignite.service.BusinessService;
import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ignite.lang.IgniteRunnable;
import org.apache.ignite.resources.SpringResource;

@Getter
@Setter
@NoArgsConstructor
public class ConsumerJob implements IgniteRunnable, Serializable {

  private static final long serialVersionUID = 0L;

  @SpringResource(resourceName = "businessService")
  private transient BusinessService businessService;
  private int messagePartitionHash;

  public ConsumerJob(int messagePartitionHash) {
    this.messagePartitionHash = messagePartitionHash;
  }

  @Override
  public void run() {
    businessService.consumeMessageQueue(messagePartitionHash).join();
  }
}
