package hu.upscale.message.store.ignite.controller;

import hu.upscale.message.store.ignite.model.Message;
import hu.upscale.message.store.ignite.service.MessageStoreService;
import hu.upscale.message.store.ignite.service.tool.MessageLogService;
import hu.upscale.message.store.ignite.service.tool.RandomMessageGeneratorService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
@Path("message-store")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageStoreController {

  private static final Logger log = LoggerFactory.getLogger(MessageStoreController.class);

  private final MessageStoreService messageStoreService;
  private final RandomMessageGeneratorService randomMessageGeneratorService;
  private final MessageLogService messageLogService;

  @GET
  public void listMessageQueues(@Suspended final AsyncResponse asyncResponse) {
    messageStoreService.getMessageQueues().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @GET
  @Path("/partitions")
  public void listMessagePartitionHashes(@Suspended final AsyncResponse asyncResponse) {
    messageStoreService.getMessagePartitionHashes().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @POST
  public void enqueueMessage(@Suspended final AsyncResponse asyncResponse, Message message) {
    messageStoreService.enqueueMessage(message).thenCombine(messageLogService.increaseMessageCounter(), (a, b) -> b)
        .handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @POST
  @Path("/random")
  public void enqueueRandomMessage(@Suspended final AsyncResponse asyncResponse) {
    enqueueMessage(asyncResponse, randomMessageGeneratorService.generateRandom());
  }

  @GET
  @Path("/statistics")
  public void getMessageStoreStatistics(@Suspended final AsyncResponse asyncResponse) {
    messageStoreService.getMessageStoreStatistics().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @GET
  @Path("/results")
  public void getMessageStoreResults(@Suspended final AsyncResponse asyncResponse) {
    messageLogService.getMessageStoreResults().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @PUT
  @Path("/clear")
  public void clear(@Suspended final AsyncResponse asyncResponse) {
    messageStoreService.clear().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  @GET
  @Path("/metrics")
  public void getMessageStoreMetrics(@Suspended final AsyncResponse asyncResponse) {
    messageStoreService.getMessageStoreMetrics().handle(this::mapResponse).thenAccept(asyncResponse::resume);
  }

  private <T> Response mapResponse(T result, Throwable error) {
    if (error != null) {
      log.error("Exception occurred during request processing", error);
      return Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
    } else if (result != null) {
      return Response.ok(result).build();
    } else {
      return Response.ok().build();
    }
  }

}
