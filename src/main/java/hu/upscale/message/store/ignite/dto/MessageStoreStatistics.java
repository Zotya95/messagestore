package hu.upscale.message.store.ignite.dto;

import hu.upscale.message.store.ignite.model.MessagePartition.MessageStatus;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MessageStoreStatistics implements Serializable {

  private static final long serialVersionUID = 0L;

  private Map<MessageStatus, Long> numberOfMessagesByStatus;

  public long getTotalNumberOfMessages() {
    return Optional.ofNullable(numberOfMessagesByStatus)
        .map(statisticsMap -> statisticsMap.values().stream().mapToLong(boxedLong -> boxedLong == null ? 0L : boxedLong)
            .sum())
        .orElse(0L);
  }

}
