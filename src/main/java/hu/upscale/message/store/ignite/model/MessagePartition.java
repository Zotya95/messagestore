package hu.upscale.message.store.ignite.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MessagePartition implements Serializable {

  private static final long serialVersionUID = 0L;

  private String messageType;
  private int processingPartition;

  @JsonIgnore
  public int getMessagePartitionHash() {
    return Objects.hash(messageType, processingPartition);
  }

  public enum MessageStatus {
    PENDING,
    IN_PROGRESS,
    FAILED,
    PROCESSED;
  }

}
