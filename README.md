#Message Store

Parkoló **Apache Ignite** alapú megoldása.

##Követelmények

* JDK11
* Internetkapcsolat a dependeciák letöltéséhez
* Cassandra a perzisztens működéshez.

## Fordítás

A repository mappájából kiadott `./gradlew clean bootJar` parancssal.

## Futtatás

A repository főkönyvtárában található egy `start.sh` script. A script 4, illetve egy opcionális ötödik paramétert vár.
Ezek:

1. Egy Cassandra node címe (host:port formátumban) pl.: `127.0.0.1:9042` 
2. Az Ignite service-hez legközelebb eső Cassandra data center neve pl.: `datacenter1`
3. A Cassandra címfordításához használt properties fájl elérési útja pl.: `cassandraHostTranslations.properties`

   Bizonyos esetekben előfordulhat, hogy a Cassandra coordinator más címeket ad meg a többi node eléréséhez,
   mint ahogyan azt a szolgáltatás meg tudná címezni.
   
   Ilyen helyzet fordulhat elő, akkor ha például a Cassandra node-ok konténerizált környezetben futnak,
   de a szolgáltatás nem. Ekkor a kordinátor a konténerek közötti címeket adja vissza,
   de a docker környezeten kívülről ezeken a címeken nem tudjuk elérni a többi Cassandra példányt.
   
   Ebben az esetben a properties fájl tartalmazza a szükséges fordításokat. (A kulcs a fordítandó cím és port, az érték a lefordított cím és port. pl.: `172.19.0.4\:9042=127.0.0.1:9043`)

4. Az Ignite service neve pl.: `iNode0`.

   Egy clusteren belül a névnek egyedinek kell lenni, ezért ha újabb példányokat szeretnénk indítani,
   akkor figyeljünk rá oda hogy azoknak különböző neveket adjunk meg!

5. Szerver port (**opcionális**) pl.: `8081`

   Ha a szolgáltatást nem konténerben indítjuk el, akkor meg kell tudnunk adni a portot,
   hogy újabb példányok indítása esetén ne ugyan azon a porton próbáljanak meg kommunikálni, az egyes process-ek.
   
   A paraméter elhagyható, mivel ha nincs megadva, akkor a `8080`-as porton indul el a szolgáltatás.
   
##### A fentiek alapján, ha három példányt szeretnénk elindítani Docker nélkül, akkor az alábbi 3 parancs segítségével tehetjük meg:

* `./start.sh 127.0.0.1:9042 datacenter1 cassandraHostTransltations.properties iNode0`
* `./start.sh 127.0.0.1:9042 datacenter1 cassandraHostTransltations.properties iNode1 8081`
* `./start.sh 127.0.0.1:9042 datacenter1 cassandraHostTransltations.properties iNode2 8082`

###### (Ha a projekt nincs lefordítva a start script futtatásakor, akkor a script elvégzi a fordítást.)