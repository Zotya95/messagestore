#!/bin/sh

printf "\n\n"

if [[ $# -lt 4 ]]; then
  printf "Not enough arguments\n\n\n"
  exit 2
elif [[ $# -gt 5 ]]; then
  printf "Too many arguments\n\n\n"
  exit 2
fi

CASSANDRA_ADDRESS=${1}
LOCAL_CASSANDRA_DATA_CENTER=${2}
CASSANDRA_ADDRESS_TRANSLATIONS_PROPS=${3}
IGNITE_INSTANCE_NAME=${4}
SERVER_PORT=${5:-8080}

printf "Parameters:\n\n\nCASSANDRA_ADDRESS=${CASSANDRA_ADDRESS}\nLOCAL_CASSANDRA_DATA_CENTER=${LOCAL_CASSANDRA_DATA_CENTER}\nCASSANDRA_ADDRESS_TRANSLATIONS_PROPS=${CASSANDRA_ADDRESS_TRANSLATIONS_PROPS}\nIGNITE_INSTANCE_NAME=${IGNITE_INSTANCE_NAME}\nSERVER_PORT=${SERVER_PORT}\n"

LIBS_DIR="build/libs"

if [[ ! -e ${LIBS_DIR} ]]; then
  printf "\n"
  ./gradlew bootJar
  printf "\n\n"
fi

BOOT_JAR=$(find ${LIBS_DIR} -type f -name \*.jar)

printf "BOOT_JAR=${BOOT_JAR}\n\n"

java -server --add-exports=java.base/jdk.internal.misc=ALL-UNNAMED --add-exports=java.base/sun.nio.ch=ALL-UNNAMED --add-exports=java.management/com.sun.jmx.mbeanserver=ALL-UNNAMED --add-exports=jdk.internal.jvmstat/sun.jvmstat.monitor=ALL-UNNAMED --add-exports=java.base/sun.reflect.generics.reflectiveObjects=ALL-UNNAMED --illegal-access=permit -Djdk.tls.client.protocols=TLSv1.2 -Djava.net.preferIPv4Stack=true -Xms2g -Xmx2g -XX:+AlwaysPreTouch -XX:+UseG1GC -XX:+ScavengeBeforeFullGC -XX:+DisableExplicitGC -DcassandraAddress=${CASSANDRA_ADDRESS} -DlocalCassandraDataCenter=${LOCAL_CASSANDRA_DATA_CENTER} -DcassandraAddressTranslationsProps=${CASSANDRA_ADDRESS_TRANSLATIONS_PROPS} -DigniteInstanceName=${IGNITE_INSTANCE_NAME} -Dserver.port=${SERVER_PORT} -jar ${BOOT_JAR}
